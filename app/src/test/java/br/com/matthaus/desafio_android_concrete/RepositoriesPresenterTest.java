package br.com.matthaus.desafio_android_concrete;

import android.os.Bundle;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import br.com.matthaus.desafio_android_concrete.models.Repositories;
import br.com.matthaus.desafio_android_concrete.models.Repository;
import br.com.matthaus.desafio_android_concrete.screens.repositories.core.RepositoriesContract;
import br.com.matthaus.desafio_android_concrete.screens.repositories.core.RepositoriesModel;
import br.com.matthaus.desafio_android_concrete.screens.repositories.core.RepositoriesPresenter;
import br.com.matthaus.desafio_android_concrete.utils.rx.TestScheduleProvider;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.TestScheduler;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Matthaus on 15/01/18.
 */

@RunWith(MockitoJUnitRunner.class)
public class RepositoriesPresenterTest {

    private RepositoriesPresenter repositoriesPresenter;
    private TestScheduleProvider testScheduleProvider;
    private TestScheduler testScheduler;

    @Mock
    RepositoriesContract.RepositoriesView repositoriesView;

    @Mock
    RepositoriesModel repositoriesModel;

    @Before
    public void initMocks() {

        testScheduler = new TestScheduler();
        testScheduleProvider = new TestScheduleProvider(testScheduler);

        CompositeDisposable compositeDisposable = new CompositeDisposable();

        repositoriesPresenter = new RepositoriesPresenter(repositoriesView, testScheduleProvider,
                repositoriesModel, compositeDisposable);

    }

    @Test
    public void loadData_isCorrect_WithData() {

        Repository repository = new Repository();
        repository.setName("Repositório");
        repository.setDescription("Descrição de repositório");

        List<Repository> repositoriesList = new ArrayList<>();
        repositoriesList.add(repository);

        Repositories repositoriesResponse = new Repositories();
        repositoriesResponse.setRepositories(repositoriesList);

        when(repositoriesModel.isNetworkAvailable()).thenReturn(true);

        doReturn(Observable.just(repositoriesResponse))
                .when(repositoriesModel)
                .getRepositories(anyInt());

        repositoriesPresenter.loadRepositoriesPage(0);

        testScheduler.triggerActions();

        verify(repositoriesView).displayLoading();
        verify(repositoriesView).hideLoading();
        verify(repositoriesView).addData(repositoriesList);

    }

    @Test
    public void loadData_showErrorMessage_WithNoInternetConnection() {

        when(repositoriesModel.isNetworkAvailable()).thenReturn(false);
        repositoriesPresenter.loadRepositoriesPage(0);

        verify(repositoriesView).displayErrorMessage(R.string.no_connection_message);

    }

    @Test
    public void loadData_showErrorMessage_WithIncorrectApiResponse() {


        when(repositoriesModel.isNetworkAvailable()).thenReturn(true);

        doReturn(Observable.just(""))
                .when(repositoriesModel)
                .getRepositories(anyInt());

        repositoriesPresenter.loadRepositoriesPage(0);

        testScheduler.triggerActions();

        verify(repositoriesView).displayLoading();
        verify(repositoriesView).hideLoading();
        verify(repositoriesView).displayErrorMessage(R.string.api_error_message);

    }


}
