package br.com.matthaus.desafio_android_concrete.app;

import android.app.Activity;
import android.app.Application;

import javax.inject.Inject;

import br.com.matthaus.desafio_android_concrete.app.dagger.DaggerTestAppComponent;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

public class DesafioAndroidConcreteAppicationTest extends Application
        implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> activityInjector;

    @Override
    public void onCreate() {
        super.onCreate();

        DaggerTestAppComponent
                .builder()
                .application(this)
                .build()
                .inject(this);

    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityInjector;
    }
}
