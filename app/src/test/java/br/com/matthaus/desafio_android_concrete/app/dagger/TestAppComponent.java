package br.com.matthaus.desafio_android_concrete.app.dagger;

import android.app.Application;

import javax.inject.Singleton;

import br.com.matthaus.desafio_android_concrete.app.DesafioAndroidConcreteAppicationTest;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class, APISupportModule.class, GithubApiModule.class})
public interface TestAppComponent extends AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        TestAppComponent build();

    }

    void inject(DesafioAndroidConcreteAppicationTest app);

}
