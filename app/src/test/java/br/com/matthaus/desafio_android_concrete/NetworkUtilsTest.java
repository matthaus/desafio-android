package br.com.matthaus.desafio_android_concrete;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.matthaus.desafio_android_concrete.utils.NetworkUtils;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class NetworkUtilsTest {

    @Mock
    private Context context;

    @Mock
    private ConnectivityManager connectivityManager;

    @Mock
    private NetworkInfo networkInfo;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void isNetworkAvailable_IsFalse_IfNullActiveNetworkInfo() {

        when(connectivityManager.getActiveNetworkInfo()).thenReturn(null);

        when(context.getSystemService(Context.CONNECTIVITY_SERVICE))
                .thenReturn(connectivityManager);

        boolean isNetworkAvailable = NetworkUtils.isNetworkAvailable(context);

        assertEquals(isNetworkAvailable, false);
    }

    @Test
    public void isNetworkAvailable_IsFalse_IfNullActiveNetworkInfoAndIsConnected() {

        when(networkInfo.isConnected()).thenReturn(false);

        when(connectivityManager.getActiveNetworkInfo()).thenReturn(networkInfo);

        when(context.getSystemService(Context.CONNECTIVITY_SERVICE))
                .thenReturn(connectivityManager);

        boolean isNetworkAvailable = NetworkUtils.isNetworkAvailable(context);

        assertEquals(isNetworkAvailable, false);
    }

    @Test
    public void isNetworkAvailable_IsTrue_IfActiveNetworkInfoAndIsConnected() {

        when(networkInfo.isConnected()).thenReturn(true);

        when(connectivityManager.getActiveNetworkInfo()).thenReturn(networkInfo);

        when(context.getSystemService(Context.CONNECTIVITY_SERVICE))
                .thenReturn(connectivityManager);

        boolean isNetworkAvailable = NetworkUtils.isNetworkAvailable(context);

        assertEquals(isNetworkAvailable, true);
    }

}
