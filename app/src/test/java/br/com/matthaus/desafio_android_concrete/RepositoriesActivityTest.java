package br.com.matthaus.desafio_android_concrete;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import br.com.matthaus.desafio_android_concrete.app.DesafioAndroidConcreteAppicationTest;
import br.com.matthaus.desafio_android_concrete.screens.repositories.RepositoriesActivity;
import br.com.matthaus.desafio_android_concrete.screens.repositories.core.RepositoriesPresenter;

/**
 * Created by matthaus on 15/01/18.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, application = DesafioAndroidConcreteAppicationTest.class)
public class RepositoriesActivityTest {

    RepositoriesActivity repositoriesActivity;

    @Mock
    RepositoriesPresenter presenter;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void repositoriesActivity_isEmpty_IfNoItems() {
        repositoriesActivity = Robolectric.setupActivity(RepositoriesActivity.class);
    }

}
