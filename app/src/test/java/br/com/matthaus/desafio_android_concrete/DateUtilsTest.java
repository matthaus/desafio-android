package br.com.matthaus.desafio_android_concrete;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Calendar;
import java.util.Date;

import br.com.matthaus.desafio_android_concrete.utils.DateUtils;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class DateUtilsTest {

    @Test
    public void parseDefaultFormatDate_isCorrect() {
        String dateStr = "2018-01-15T18:44:33Z";
        Date date = DateUtils.parseDefaultFormatDate(dateStr);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        assertEquals(calendar.get(Calendar.YEAR), 2018);
        assertEquals(calendar.get(Calendar.MONTH), 0);
        assertEquals(calendar.get(Calendar.DAY_OF_MONTH), 15);
        assertEquals(calendar.get(Calendar.HOUR_OF_DAY), 18);
        assertEquals(calendar.get(Calendar.MINUTE), 44);
        assertEquals(calendar.get(Calendar.SECOND), 33);

    }

    @Test
    public void convertDefaultBrazilianDateString_isCorrect() {
        String dateStr = "2018-01-15T18:44:33Z";
        Date date = DateUtils.parseDefaultFormatDate(dateStr);

        String convertedString = DateUtils.convertDateToBrazilianString(date);

        assertEquals(convertedString, "15/01/2018 18:44");
    }

}
