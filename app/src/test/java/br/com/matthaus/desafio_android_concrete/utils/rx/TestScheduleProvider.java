package br.com.matthaus.desafio_android_concrete.utils.rx;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.TestScheduler;

public class TestScheduleProvider implements SchedulerProvider {

    private final TestScheduler scheduler;

    public TestScheduleProvider(TestScheduler scheduler) {
        this.scheduler = scheduler;
    }

    @Override
    public Scheduler io() {
        return scheduler;
    }

    @Override
    public Scheduler ui() {
        return scheduler;
    }
}
