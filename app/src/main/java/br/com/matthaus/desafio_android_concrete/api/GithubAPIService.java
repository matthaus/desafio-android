package br.com.matthaus.desafio_android_concrete.api;

import java.util.List;

import br.com.matthaus.desafio_android_concrete.models.PullRequest;
import br.com.matthaus.desafio_android_concrete.models.Repositories;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GithubAPIService {

    @GET("search/repositories?q=language:Java&sort=stars")
    Observable<Repositories> getRepositories(@Query("page") int page);

    @GET("repos/{owner}/{repository}/pulls")
    Observable<List<PullRequest>> getPullRequests(@Path("owner") String owner,
                                                  @Path("repository") String repository);

}
