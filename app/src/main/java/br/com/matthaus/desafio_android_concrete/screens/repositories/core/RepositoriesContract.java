package br.com.matthaus.desafio_android_concrete.screens.repositories.core;

import android.os.Bundle;

import java.util.List;

import br.com.matthaus.desafio_android_concrete.models.Repository;

public interface RepositoriesContract {

    interface RepositoriesView {
        void addData(List<Repository> repositories);
        void setNoMoreItems(boolean noMoreItems);

        void displayLoading();
        void hideLoading();
        void displayErrorMessage(int errorMessage);

        void openRepositoryActivity(Bundle bundle);
    }

    interface RepositoriesPresenter {
        void loadRepositoriesPage(int page);
        void onRepositorySelected(String repositoryOwner, String repositoryName);
    }

}
