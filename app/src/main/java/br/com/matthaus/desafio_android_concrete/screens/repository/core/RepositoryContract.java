package br.com.matthaus.desafio_android_concrete.screens.repository.core;

import java.util.List;

import br.com.matthaus.desafio_android_concrete.models.PullRequest;

/**
 * Created by Matthaus on 15/01/18.
 */

public interface RepositoryContract {

    interface RepositoryView {
        void setData(List<PullRequest> pullRequests);
        void displayLoading();
        void hideLoading();
        void displayErrorMessage(int errorMessage);
    }

    interface RepositoryPresenter {
        void loadPullRequests(String owner, String repository);
    }

}
