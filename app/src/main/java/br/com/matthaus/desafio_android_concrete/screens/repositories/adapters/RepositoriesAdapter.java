package br.com.matthaus.desafio_android_concrete.screens.repositories.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.matthaus.desafio_android_concrete.R;
import br.com.matthaus.desafio_android_concrete.models.Repository;
import io.reactivex.annotations.NonNull;

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoryViewHolder>
        implements RepositoryViewHolder.OnRepositoryClickListener {

    private Context context;
    private List<Repository> repositories;
    private OnRepositorySelectedListener onRepositorySelectedListener;

    public RepositoriesAdapter(Context context, @NonNull List<Repository> repositories,
                               OnRepositorySelectedListener onRepositorySelectedListener) {
        this.context = context;
        this.repositories = repositories;
        this.onRepositorySelectedListener = onRepositorySelectedListener;
    }

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_repositories, parent,
                false);

        RepositoryViewHolder repositoryViewHolder = new RepositoryViewHolder(view, this);
        return repositoryViewHolder;
    }

    @Override
    public void onBindViewHolder(RepositoryViewHolder holder, int position) {
        Repository repository = repositories.get(position);
        holder.bind(repository);
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    @Override
    public void onRepositoryClick(int position) {
        if (onRepositorySelectedListener != null) {
            onRepositorySelectedListener.onRepositorySelected(
                    repositories.get(position).getOwner().getLogin(),
                    repositories.get(position).getName());
        }
    }

    public void addData(List<Repository> repositories) {
        this.repositories.addAll(repositories);
    }

    public interface OnRepositorySelectedListener {
        void onRepositorySelected(String repositoryOwner, String repositoryName);
    }

}
