package br.com.matthaus.desafio_android_concrete.app.dagger;

import br.com.matthaus.desafio_android_concrete.screens.repositories.RepositoriesActivity;
import br.com.matthaus.desafio_android_concrete.screens.repositories.dagger.RepositoriesModule;
import br.com.matthaus.desafio_android_concrete.screens.repositories.dagger.RepositoriesViewModule;
import br.com.matthaus.desafio_android_concrete.screens.repository.RepositoryActivity;
import br.com.matthaus.desafio_android_concrete.screens.repository.dagger.RepositoryModule;
import br.com.matthaus.desafio_android_concrete.screens.repository.dagger.RepositoryViewModule;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(modules = {RepositoriesViewModule.class, RepositoriesModule.class})
    abstract RepositoriesActivity bindRepositoriesActivity();

    @ContributesAndroidInjector(modules = {RepositoryViewModule.class, RepositoryModule.class})
    abstract RepositoryActivity bindRepositoryActivity();

}
