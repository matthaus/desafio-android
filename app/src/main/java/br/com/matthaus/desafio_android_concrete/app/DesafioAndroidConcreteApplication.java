package br.com.matthaus.desafio_android_concrete.app;

import android.app.Activity;
import android.app.Application;

import javax.inject.Inject;

import br.com.matthaus.desafio_android_concrete.app.dagger.AppComponent;
import br.com.matthaus.desafio_android_concrete.app.dagger.DaggerAppComponent;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

public class DesafioAndroidConcreteApplication extends Application implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> activityInjector;

    @Override
    public void onCreate() {
        super.onCreate();

        DaggerAppComponent
                .builder()
                .application(this)
                .build()
                .inject(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityInjector;
    }
}
