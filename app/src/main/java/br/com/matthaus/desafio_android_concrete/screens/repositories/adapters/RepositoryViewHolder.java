package br.com.matthaus.desafio_android_concrete.screens.repositories.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.matthaus.desafio_android_concrete.R;
import br.com.matthaus.desafio_android_concrete.models.Repository;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RepositoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private OnRepositoryClickListener onRepositoryClickListener;

    @BindView(R.id.tvRepositoryName)
    TextView textViewRepositoryName;

    @BindView(R.id.tvRepositoryDescription)
    TextView textViewRepositoryDescription;

    @BindView(R.id.tvForkCount)
    TextView textViewForkCount;

    @BindView(R.id.tvStarCount)
    TextView textViewStarCount;

    @BindView(R.id.ivAvatar)
    ImageView imageViewAvatar;

    @BindView(R.id.tvUsername)
    TextView textViewUsername;

    @BindView(R.id.tvNameSurname)
    TextView textViewNameSurname;

    View rootView;

    public RepositoryViewHolder(View view, OnRepositoryClickListener onRepositoryClickListener) {
        super(view);

        view.setOnClickListener(this);

        this.rootView = view;
        this.onRepositoryClickListener = onRepositoryClickListener;

        ButterKnife.bind(this, view);
    }

    public void bind(Repository repository) {
        textViewRepositoryName.setText(repository.getName());
        textViewRepositoryDescription.setText(repository.getDescription());
        textViewForkCount.setText(repository.getForksCount().toString());
        textViewStarCount.setText(repository.getStargazersCount().toString());
        textViewUsername.setText(repository.getOwner().getLogin());
        textViewNameSurname.setText(repository.getFullName());

        Picasso.with(rootView.getContext())
                .load(repository.getOwner().getAvatarUrl())
                .placeholder(R.drawable.ic_avatar)
                .error(R.drawable.ic_avatar)
                .into(imageViewAvatar);

    }

    @Override
    public void onClick(View view) {
        if (onRepositoryClickListener != null) {
            onRepositoryClickListener.onRepositoryClick(getAdapterPosition());
        }
    }

    public interface OnRepositoryClickListener {
        void onRepositoryClick(int position);
    }

}
