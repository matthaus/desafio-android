package br.com.matthaus.desafio_android_concrete.screens.repositories.dagger;

import br.com.matthaus.desafio_android_concrete.screens.repositories.RepositoriesActivity;
import br.com.matthaus.desafio_android_concrete.screens.repositories.core.RepositoriesContract;
import dagger.Binds;
import dagger.Module;

@Module
public abstract class RepositoriesViewModule {

    @Binds
    abstract RepositoriesContract.RepositoriesView proviveRepositoriesView(
            RepositoriesActivity repositoriesActivity);

}
