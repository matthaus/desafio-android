package br.com.matthaus.desafio_android_concrete.screens.repositories.core;

import android.content.Context;

import br.com.matthaus.desafio_android_concrete.api.GithubAPIService;
import br.com.matthaus.desafio_android_concrete.models.Repositories;
import br.com.matthaus.desafio_android_concrete.screens.repositories.RepositoriesActivity;
import br.com.matthaus.desafio_android_concrete.utils.NetworkUtils;
import io.reactivex.Observable;

public class RepositoriesModel {

    private Context context;
    private GithubAPIService githubAPIService;

    public RepositoriesModel(Context context, GithubAPIService api) {
        this.context = context;
        this.githubAPIService = api;
    }

    public Observable<Repositories> getRepositories(int page) {
        return githubAPIService.getRepositories(page);
    }

    public Boolean isNetworkAvailable() {
        return NetworkUtils.isNetworkAvailable(context);
    }


}
