package br.com.matthaus.desafio_android_concrete.app.dagger;

import br.com.matthaus.desafio_android_concrete.api.GithubAPIService;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class GithubApiModule {

    private static final String API_URL = "https://api.github.com/";

    @Provides
    GithubAPIService provideGithubAPI(OkHttpClient client, GsonConverterFactory gson, RxJava2CallAdapterFactory rxAdapter) {
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .baseUrl(API_URL)
                .addConverterFactory(gson)
                .addCallAdapterFactory(rxAdapter)
                .build();

        return retrofit.create(GithubAPIService.class);
    }

}
