package br.com.matthaus.desafio_android_concrete.screens.repository.dagger;

import br.com.matthaus.desafio_android_concrete.screens.repository.RepositoryActivity;
import br.com.matthaus.desafio_android_concrete.screens.repository.core.RepositoryContract;
import dagger.Binds;
import dagger.Module;

@Module
public abstract class RepositoryViewModule {

    @Binds
    abstract RepositoryContract.RepositoryView proviveRepositoriesView(
            RepositoryActivity reposyActivity);

}
