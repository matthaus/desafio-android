package br.com.matthaus.desafio_android_concrete.screens.repository.dagger;

import android.content.Context;

import br.com.matthaus.desafio_android_concrete.api.GithubAPIService;
import br.com.matthaus.desafio_android_concrete.screens.repository.core.RepositoryContract;
import br.com.matthaus.desafio_android_concrete.screens.repository.core.RepositoryModel;
import br.com.matthaus.desafio_android_concrete.screens.repository.core.RepositoryPresenter;
import br.com.matthaus.desafio_android_concrete.utils.rx.SchedulerProvider;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class RepositoryModule {

    @Provides
    RepositoryModel provideModel(Context context,
                                 GithubAPIService api) {
        return new RepositoryModel(context, api);
    }


    @Provides
    RepositoryPresenter providePresenter(RepositoryContract.RepositoryView view,
                                         SchedulerProvider schedulerProvider,
                                         RepositoryModel model) {
        return new RepositoryPresenter(view, schedulerProvider, model, new CompositeDisposable());
    }

}
