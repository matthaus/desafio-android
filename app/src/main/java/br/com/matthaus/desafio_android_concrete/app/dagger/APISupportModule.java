package br.com.matthaus.desafio_android_concrete.app.dagger;

import android.content.Context;

import java.io.File;

import br.com.matthaus.desafio_android_concrete.utils.rx.AppScheduleProvider;
import br.com.matthaus.desafio_android_concrete.utils.rx.SchedulerProvider;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class APISupportModule {

    public static final int CACHE_SIZE = 5 * 1024 * 1024;

    @Provides
    OkHttpClient provideHttpClient(Context context) {

        File cacheDir = new File(context.getCacheDir(), "http");
        Cache cache = new Cache(cacheDir, CACHE_SIZE);

        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.cache(cache);

        return builder.build();
    }

    @Provides
    GsonConverterFactory provideGsonClient() {
        return GsonConverterFactory.create();
    }

    @Provides
    RxJava2CallAdapterFactory provideRxAdapter() {
        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppScheduleProvider();
    }

}