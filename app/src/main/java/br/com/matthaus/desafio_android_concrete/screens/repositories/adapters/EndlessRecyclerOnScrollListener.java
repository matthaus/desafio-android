package br.com.matthaus.desafio_android_concrete.screens.repositories.adapters;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public abstract class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {

    private LinearLayoutManager linearLayoutManager;
    private boolean loading = false;
    private int totalEntries = 0;
    private int previousItemCount = 0;
    private int currentPage = 0;
    private boolean hasMore = true;

    public EndlessRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager) {
        this.linearLayoutManager = linearLayoutManager;
    }

    public abstract void onLoadMore(int currentPage);

    public void setHasMore(boolean hasMore) {
        this.hasMore = hasMore;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = recyclerView.getChildCount();
        int totalItemCount = linearLayoutManager.getItemCount();
        int firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

        if (loading) {
            int diffItens = totalItemCount - previousItemCount;

            if ((diffItens > 1) || totalItemCount >= totalEntries) {
                loading = false;
                previousItemCount = totalItemCount;
            }
        } else {
            if (hasMore) {
                if ((firstVisibleItem + visibleItemCount) >= totalItemCount) {
                    onLoadMore(++currentPage);
                    loading = true;
                    previousItemCount = totalItemCount;
                }
            }
        }

    }
}
