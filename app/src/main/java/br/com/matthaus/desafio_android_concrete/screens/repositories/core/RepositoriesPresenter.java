package br.com.matthaus.desafio_android_concrete.screens.repositories.core;

import android.content.Intent;
import android.os.Bundle;

import java.util.List;

import br.com.matthaus.desafio_android_concrete.R;
import br.com.matthaus.desafio_android_concrete.models.Repositories;
import br.com.matthaus.desafio_android_concrete.models.Repository;
import br.com.matthaus.desafio_android_concrete.utils.rx.SchedulerProvider;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class RepositoriesPresenter implements RepositoriesContract.RepositoriesPresenter {

    private final static int REPOSITORIES_PER_PAGE = 30;

    public final static String EXTRA_REPOSITORY_NAME_ID = "extra_repository_name";
    public final static String EXTRA_REPOSITORY_OWNER_ID = "extra_repository_owner";

    private RepositoriesContract.RepositoriesView repositoriesView;
    private RepositoriesModel repositoriesModel;
    private CompositeDisposable compositeDisposable;
    private SchedulerProvider schedulerProvider;

    public RepositoriesPresenter(RepositoriesContract.RepositoriesView view,
                                 SchedulerProvider schedulerProvider, RepositoriesModel model,
                                 CompositeDisposable composite) {
        this.repositoriesView = view;
        this.repositoriesModel = model;
        this.compositeDisposable = composite;
        this.schedulerProvider = schedulerProvider;
    }

    @Override
    public void loadRepositoriesPage(int page) {
        if (repositoriesModel.isNetworkAvailable()) {
            repositoriesView.displayLoading();
            compositeDisposable.add(getRepositoriesList(page));
        } else {
            repositoriesView.displayErrorMessage(R.string.no_connection_message);
        }
    }

    @Override
    public void onRepositorySelected(String repositoryOwner, String repositoryName) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_REPOSITORY_NAME_ID, repositoryName);
        bundle.putString(EXTRA_REPOSITORY_OWNER_ID, repositoryOwner);

        repositoriesView.openRepositoryActivity(bundle);
    }

    private Disposable getRepositoriesList(int page) {
        return repositoriesModel
                .getRepositories(page)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .map(new Function<Repositories, List<Repository>>() {
                    @Override
                    public List<Repository> apply(Repositories repositories) throws Exception {
                        return repositories.getRepositories();
                    }
                })
                .subscribe(new Consumer<List<Repository>>() {
                    @Override
                    public void accept(List<Repository> repositories) throws Exception {
                        repositoriesView.addData(repositories);
                        repositoriesView.hideLoading();

                        if (repositories.size() < REPOSITORIES_PER_PAGE) {
                            repositoriesView.setNoMoreItems(false);
                        } else {
                            repositoriesView.setNoMoreItems(true);
                        }

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        repositoriesView.hideLoading();
                        repositoriesView.displayErrorMessage(R.string.api_error_message);
                    }
                });
    }

}
