package br.com.matthaus.desafio_android_concrete.screens.repository.core;

import android.content.Context;

import java.util.List;

import br.com.matthaus.desafio_android_concrete.api.GithubAPIService;
import br.com.matthaus.desafio_android_concrete.models.PullRequest;
import br.com.matthaus.desafio_android_concrete.utils.NetworkUtils;
import io.reactivex.Observable;

/**
 * Created by Matthaus on 15/01/18.
 */

public class RepositoryModel {

    private Context context;
    private GithubAPIService githubAPIService;

    public RepositoryModel(Context context, GithubAPIService api) {
        this.context = context;
        this.githubAPIService = api;
    }

    public Observable<List<PullRequest>> getPullRequests(String owner, String repository) {
        return githubAPIService.getPullRequests(owner, repository);
    }

    public Boolean isNetworkAvailable() {
        return NetworkUtils.isNetworkAvailable(context);
    }

}
