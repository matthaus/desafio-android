package br.com.matthaus.desafio_android_concrete.screens.repository.core;

import java.util.List;

import br.com.matthaus.desafio_android_concrete.R;
import br.com.matthaus.desafio_android_concrete.models.PullRequest;
import br.com.matthaus.desafio_android_concrete.screens.repositories.core.RepositoriesContract;
import br.com.matthaus.desafio_android_concrete.utils.rx.SchedulerProvider;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Matthaus on 15/01/18.
 */

public class RepositoryPresenter implements RepositoryContract.RepositoryPresenter {

    private RepositoryContract.RepositoryView repositoryView;
    private RepositoryModel repositoryModel;
    private CompositeDisposable compositeDisposable;
    private SchedulerProvider schedulerProvider;

    public RepositoryPresenter(RepositoryContract.RepositoryView view,
                                 SchedulerProvider schedulerProvider, RepositoryModel model,
                                 CompositeDisposable composite) {
        this.repositoryView = view;
        this.repositoryModel = model;
        this.compositeDisposable = composite;
        this.schedulerProvider = schedulerProvider;
    }

    @Override
    public void loadPullRequests(String owner, String repository) {
        if (repositoryModel.isNetworkAvailable()) {
            repositoryView.displayLoading();
            compositeDisposable.add(getRepositoryPullRequests(owner, repository));
        } else {
            repositoryView.displayErrorMessage(R.string.no_connection_message);
        }
    }

    private Disposable getRepositoryPullRequests(String owner, String repository) {
        return repositoryModel
                .getPullRequests(owner, repository)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(new Consumer<List<PullRequest>>() {
                    @Override
                    public void accept(List<PullRequest> pullRequests) throws Exception {
                        repositoryView.hideLoading();
                        repositoryView.setData(pullRequests);

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        repositoryView.hideLoading();
                        repositoryView.displayErrorMessage(R.string.api_error_message);
                    }
                });
    }

}
