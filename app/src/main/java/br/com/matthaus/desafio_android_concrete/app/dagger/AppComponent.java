package br.com.matthaus.desafio_android_concrete.app.dagger;

import android.app.Application;

import javax.inject.Singleton;

import br.com.matthaus.desafio_android_concrete.app.DesafioAndroidConcreteApplication;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class, APISupportModule.class, GithubApiModule.class})
public interface AppComponent extends AndroidInjector<DesafioAndroidConcreteApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();

    }

    void inject(DesafioAndroidConcreteApplication app);

}
