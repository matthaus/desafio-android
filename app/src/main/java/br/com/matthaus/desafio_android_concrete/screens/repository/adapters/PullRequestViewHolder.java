package br.com.matthaus.desafio_android_concrete.screens.repository.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.matthaus.desafio_android_concrete.R;
import br.com.matthaus.desafio_android_concrete.models.PullRequest;
import br.com.matthaus.desafio_android_concrete.utils.DateUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PullRequestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private OnPullRequestClickListener onPullRequestClickListener;

    @BindView(R.id.tvPullRequestTitle)
    TextView textViewPullRequestTitle;

    @BindView(R.id.tvPullRequestDescription)
    TextView textViewPullRequestDescription;

    @BindView(R.id.ivAvatar)
    ImageView imageViewAvatar;

    @BindView(R.id.tvUsername)
    TextView textViewUsername;

    @BindView(R.id.tvPullRequestDate)
    TextView textViewPullRequestDate;

    View rootView;

    public PullRequestViewHolder(View view, OnPullRequestClickListener onPullRequestClickListener) {
        super(view);

        view.setOnClickListener(this);

        this.rootView = view;
        this.onPullRequestClickListener = onPullRequestClickListener;

        ButterKnife.bind(this, view);
    }

    public void bind(PullRequest pullRequest) {
        textViewPullRequestTitle.setText(pullRequest.getTitle());
        textViewPullRequestDescription.setText(pullRequest.getBody());
        textViewUsername.setText(pullRequest.getUser().getLogin());
        textViewPullRequestDate.setText(DateUtils.convertDateToBrazilianString(
                DateUtils.parseDefaultFormatDate(pullRequest.getCreatedAt())));

        Picasso.with(rootView.getContext())
                .load(pullRequest.getUser().getAvatarUrl())
                .placeholder(R.drawable.ic_avatar)
                .error(R.drawable.ic_avatar)
                .into(imageViewAvatar);

    }

    @Override
    public void onClick(View view) {
        if (onPullRequestClickListener != null) {
            onPullRequestClickListener.onPullRequestClick(getAdapterPosition());
        }
    }

    public interface OnPullRequestClickListener {
        void onPullRequestClick(int position);
    }

}
