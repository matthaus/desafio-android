package br.com.matthaus.desafio_android_concrete.app.dagger;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

@Module(includes = ActivityBuilderModule.class)
public class AppModule {

    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

}
