package br.com.matthaus.desafio_android_concrete.screens.repositories;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.matthaus.desafio_android_concrete.R;
import br.com.matthaus.desafio_android_concrete.models.Repository;
import br.com.matthaus.desafio_android_concrete.screens.repositories.adapters.EndlessRecyclerOnScrollListener;
import br.com.matthaus.desafio_android_concrete.screens.repositories.adapters.RepositoriesAdapter;
import br.com.matthaus.desafio_android_concrete.screens.repositories.core.RepositoriesContract;
import br.com.matthaus.desafio_android_concrete.screens.repositories.core.RepositoriesPresenter;
import br.com.matthaus.desafio_android_concrete.screens.repository.RepositoryActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

public class RepositoriesActivity extends AppCompatActivity
        implements RepositoriesContract.RepositoriesView,
        RepositoriesAdapter.OnRepositorySelectedListener {

    private static final int FIRST_PAGE = 0;

    private EndlessRecyclerOnScrollListener endlessRecyclerOnScrollListener;
    private LinearLayoutManager repositoriesLayoutManager;
    private RepositoriesAdapter repositoriesAdapter;

    @Inject
    RepositoriesPresenter presenter;

    @BindView(R.id.repositoriesActivityContainer)
    ConstraintLayout repositoriesActivityContainer;

    @BindView(R.id.rvRepositories)
    RecyclerView recyclerViewRepositories;

    @BindView(R.id.clLoader)
    ConstraintLayout constraintLayoutLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_repositories);
        ButterKnife.bind(this);

        loadData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void addData(List<Repository> repositories) {
        repositoriesAdapter.addData(repositories);
        repositoriesAdapter.notifyDataSetChanged();
    }

    @Override
    public void setNoMoreItems(boolean noMoreItems) {
        endlessRecyclerOnScrollListener.setHasMore(noMoreItems);
    }

    @Override
    public void displayLoading() {
        constraintLayoutLoader.setVisibility(View.VISIBLE);
        recyclerViewRepositories.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        constraintLayoutLoader.setVisibility(View.GONE);
        recyclerViewRepositories.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayErrorMessage(int errorMessage) {
        Snackbar snackbarError = Snackbar.make(repositoriesActivityContainer, errorMessage,
                Snackbar.LENGTH_LONG);
        hideLoading();
        snackbarError.show();
    }

    @Override
    public void openRepositoryActivity(Bundle bundle) {
        Intent intent = new Intent(this, RepositoryActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onRepositorySelected(String repositoryOwner, String repositoryName) {
        presenter.onRepositorySelected(repositoryOwner, repositoryName);
    }

    private void loadData() {
        initRecyclerView();
        presenter.loadRepositoriesPage(FIRST_PAGE);
    }

    private void initRecyclerView() {

        repositoriesLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);

        endlessRecyclerOnScrollListener =
                new EndlessRecyclerOnScrollListener(repositoriesLayoutManager) {
                    @Override
                    public void onLoadMore(int currentPage) {
                        if (repositoriesAdapter != null) {
                            repositoriesAdapter.notifyItemInserted(
                                    repositoriesAdapter.getItemCount());
                            presenter.loadRepositoriesPage(currentPage);
                        }
                    }
                };

        repositoriesAdapter = new RepositoriesAdapter(this, new ArrayList<Repository>(),
                this);

        recyclerViewRepositories.setLayoutManager(repositoriesLayoutManager);
        recyclerViewRepositories.setHasFixedSize(true);
        recyclerViewRepositories.setAdapter(repositoriesAdapter);
        recyclerViewRepositories.addOnScrollListener(endlessRecyclerOnScrollListener);

    }

}
