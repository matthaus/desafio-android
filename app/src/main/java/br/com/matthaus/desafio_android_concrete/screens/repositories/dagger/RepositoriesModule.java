package br.com.matthaus.desafio_android_concrete.screens.repositories.dagger;

import android.content.Context;

import br.com.matthaus.desafio_android_concrete.api.GithubAPIService;
import br.com.matthaus.desafio_android_concrete.screens.repositories.core.RepositoriesContract;
import br.com.matthaus.desafio_android_concrete.screens.repositories.core.RepositoriesModel;
import br.com.matthaus.desafio_android_concrete.screens.repositories.core.RepositoriesPresenter;
import br.com.matthaus.desafio_android_concrete.utils.rx.SchedulerProvider;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class RepositoriesModule {

    @Provides
    RepositoriesModel provideModel(Context context,
                                   GithubAPIService api) {
        return new RepositoriesModel(context, api);
    }


    @Provides
    RepositoriesPresenter providePresenter(RepositoriesContract.RepositoriesView view,
                                           SchedulerProvider schedulerProvider,
                                           RepositoriesModel model) {
        return new RepositoriesPresenter(view, schedulerProvider, model, new CompositeDisposable());
    }

}
