package br.com.matthaus.desafio_android_concrete.screens.repository;

import android.content.Intent;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.matthaus.desafio_android_concrete.R;
import br.com.matthaus.desafio_android_concrete.models.PullRequest;
import br.com.matthaus.desafio_android_concrete.screens.repositories.adapters.RepositoriesAdapter;
import br.com.matthaus.desafio_android_concrete.screens.repositories.core.RepositoriesPresenter;
import br.com.matthaus.desafio_android_concrete.screens.repository.adapters.PullRequestsAdapter;
import br.com.matthaus.desafio_android_concrete.screens.repository.core.RepositoryContract;
import br.com.matthaus.desafio_android_concrete.screens.repository.core.RepositoryPresenter;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

public class RepositoryActivity extends AppCompatActivity
        implements RepositoryContract.RepositoryView, PullRequestsAdapter.OnPullRequestSelectedListener {

    private String repositoryOwner;
    private String repositoryName;

    private LinearLayoutManager pullRequestsLayoutManager;
    private PullRequestsAdapter pullRequestsAdapter;

    @Inject
    RepositoryPresenter presenter;

    @BindView(R.id.repositoryActivityContainer)
    ConstraintLayout repositoryActivityContainer;

    @BindView(R.id.llCounters)
    LinearLayout linearLayoutCounters;

    @BindView(R.id.rvPullRequests)
    RecyclerView recyclerViewPullRequests;

    @BindView(R.id.clLoader)
    ConstraintLayout constraintLayoutLoader;

    @BindView(R.id.tvOpenedPullRequests)
    TextView textViewOpenedPullRequests;

    @BindView(R.id.tvClosedPullRequests)
    TextView textViewClosedPullRequests;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_repository);
        ButterKnife.bind(this);

        repositoryOwner = getIntent().getStringExtra(RepositoriesPresenter
                .EXTRA_REPOSITORY_OWNER_ID);
        repositoryName = getIntent().getStringExtra(RepositoriesPresenter
                .EXTRA_REPOSITORY_NAME_ID);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(repositoryName);

        loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    @Override
    public void setData(List<PullRequest> pullRequests) {
        pullRequestsAdapter.setData(pullRequests);
        pullRequestsAdapter.notifyDataSetChanged();

        textViewOpenedPullRequests.setText(String.format(getString(R.string.opened_pull_requests),
                pullRequestsAdapter.getOpenedPullRequests()));
        textViewClosedPullRequests.setText(String.format(getString(R.string.closed_pull_requests),
                pullRequestsAdapter.getClosedPullRequests()));

    }

    @Override
    public void displayLoading() {
        constraintLayoutLoader.setVisibility(View.VISIBLE);
        recyclerViewPullRequests.setVisibility(View.GONE);
        linearLayoutCounters.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        constraintLayoutLoader.setVisibility(View.GONE);
        recyclerViewPullRequests.setVisibility(View.VISIBLE);
        linearLayoutCounters.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayErrorMessage(int errorMessage) {
        Snackbar snackbarError = Snackbar.make(repositoryActivityContainer, errorMessage,
                Snackbar.LENGTH_LONG);
        hideLoading();
        snackbarError.show();
    }

    @Override
    public void onPullRequestSelected(String number) {
        Uri uri = Uri.parse(String.format(getString(R.string.url_github_pull_request),
                repositoryOwner, repositoryName, number));
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(browserIntent);
    }

    private void loadData() {
        initRecyclerView();
        presenter.loadPullRequests(repositoryOwner, repositoryName);
    }

    private void initRecyclerView() {

        pullRequestsLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);

        pullRequestsAdapter = new PullRequestsAdapter(this, new ArrayList<PullRequest>(),
                this);

        recyclerViewPullRequests.setLayoutManager(pullRequestsLayoutManager);
        recyclerViewPullRequests.setHasFixedSize(true);
        recyclerViewPullRequests.setAdapter(pullRequestsAdapter);

    }

}
