package br.com.matthaus.desafio_android_concrete.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    private static final String DEFAULT_PARSE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static final String BRAZILIAN_FORMAT = "dd/MM/yyyy HH:mm";

    public static Date parseDefaultFormatDate(String date) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(DEFAULT_PARSE_FORMAT);
            return format.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String convertDateToBrazilianString(Date date) {
        SimpleDateFormat format = new SimpleDateFormat(BRAZILIAN_FORMAT);
        return format.format(date);
    }

}
