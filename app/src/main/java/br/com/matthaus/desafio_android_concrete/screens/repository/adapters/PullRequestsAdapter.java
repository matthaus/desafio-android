package br.com.matthaus.desafio_android_concrete.screens.repository.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.matthaus.desafio_android_concrete.R;
import br.com.matthaus.desafio_android_concrete.models.PullRequest;
import br.com.matthaus.desafio_android_concrete.models.Repository;

public class PullRequestsAdapter extends RecyclerView.Adapter<PullRequestViewHolder>
        implements PullRequestViewHolder.OnPullRequestClickListener {

    private Context context;
    private List<PullRequest> pullRequests;
    private OnPullRequestSelectedListener onPullRequestSelectedListener;
    private int openedPullRequests, closedPullRequests;

    public PullRequestsAdapter(Context context, @NonNull List<PullRequest> pullRequests,
                               OnPullRequestSelectedListener onPullRequestSelectedListener) {
        this.context = context;
        this.pullRequests = pullRequests;
        this.onPullRequestSelectedListener = onPullRequestSelectedListener;
    }

    @Override
    public PullRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_pull_requests, parent,
                false);

        PullRequestViewHolder repositoryViewHolder = new PullRequestViewHolder(view, this);
        return repositoryViewHolder;
    }

    @Override
    public void onBindViewHolder(PullRequestViewHolder holder, int position) {
        PullRequest repository = pullRequests.get(position);
        holder.bind(repository);
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    @Override
    public void onPullRequestClick(int position) {
        if (onPullRequestSelectedListener != null) {
            onPullRequestSelectedListener.onPullRequestSelected(
                    pullRequests.get(position).getNumber().toString());
        }
    }

    public void setData(List<PullRequest> pullRequests) {
        this.pullRequests.addAll(pullRequests);
        countPullRequests();
    }

    public String getOpenedPullRequests() {
        return String.valueOf(openedPullRequests);
    }

    public String getClosedPullRequests() {
        return String.valueOf(closedPullRequests);
    }

    private void countPullRequests() {
        for (PullRequest pullRequest: pullRequests) {
            if (pullRequest.getState().equals("open"))
                openedPullRequests++;
            else
                closedPullRequests++;
        }
    }

    public interface OnPullRequestSelectedListener {
        void onPullRequestSelected(String number);
    }

}
