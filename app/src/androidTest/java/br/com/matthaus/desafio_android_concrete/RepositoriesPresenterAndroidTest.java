package br.com.matthaus.desafio_android_concrete;

import android.os.Bundle;
import android.support.test.runner.AndroidJUnit4;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import br.com.matthaus.desafio_android_concrete.screens.repositories.core.RepositoriesContract;
import br.com.matthaus.desafio_android_concrete.screens.repositories.core.RepositoriesPresenter;
import io.reactivex.disposables.CompositeDisposable;


import static org.mockito.Mockito.verify;

@RunWith(AndroidJUnit4.class)
public class RepositoriesPresenterAndroidTest {

    private RepositoriesPresenter repositoriesPresenter;

    @Mock
    RepositoriesContract.RepositoriesView repositoriesView;

    @Before
    public void initMocks() {

        MockitoAnnotations.initMocks(this);

        CompositeDisposable compositeDisposable = new CompositeDisposable();

        repositoriesPresenter = new RepositoriesPresenter(repositoriesView, null,
                null, compositeDisposable);

    }

    @Test
    public void repositorySelected_IsIntentParametersCorrect() {

        ArgumentCaptor<Bundle> bundleCaptor = ArgumentCaptor.forClass(Bundle.class);

        repositoriesPresenter.onRepositorySelected("dono_repositorio", "nome_repositorio");

        verify(repositoriesView).openRepositoryActivity(bundleCaptor.capture());

        assertEquals(bundleCaptor.getValue().getString("extra_repository_name"),
                "nome_repositorio");

        assertEquals(bundleCaptor.getValue().getString("extra_repository_owner"),
                "dono_repositorio");

    }

}
